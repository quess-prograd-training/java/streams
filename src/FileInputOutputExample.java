import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

public class FileInputOutputExample {
    void display() {
        int i=0;
        try {
            FileInputStream fileInputStream = new FileInputStream("D:\\ProGrad Training1\\NewTest.txt");
            while((i=fileInputStream.read())!=-1){
                System.out.print((char) i);
            }
        }
        catch(Exception exception){
            System.out.println(exception.getMessage());
        }
    }
    void write(){
        try{
            FileOutputStream fileOutputStream= new FileOutputStream("D:\\ProGrad Training1\\NewTest1.txt");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.next();
            byte[] array=input.getBytes();
            fileOutputStream.write(array);
            fileOutputStream.close();
        }
        catch(Exception exception){
            System.out.println(exception.getMessage());
        }
    }
}
